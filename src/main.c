/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include <pspsdk.h>
#include <pspkernel.h>
#include <pspsysmem_kernel.h>
#include <pspsyscon2.h>
#include <pspmodulemgr_kernel.h>

#include <psplflash_fatfmt.h>
#include <infinity_kinstaller.h>

#include <string.h>

#include "pspdecrypt.h"

PSP_MODULE_INFO("infinity_kinstaller", 0x1007, 1, 0);

#define UPDATE_MAGIC    "INFINITY UPDATE"
#define MFC_MAGIC       0x43464D00

int sceUtilsBufferCopyWithRange(u8 *out, unsigned int outsize, u8 *in, unsigned int insize, int cmd);

const u8 g_davee_ecdsa_pub[] =
{
    0xD6, 0x61, 0x3C, 0xB8, 0xC4, 0x80, 0xC7, 0x80,
    0x12, 0x6C, 0x33, 0x1F, 0x0E, 0x31, 0xD9, 0x6E,
    0x58, 0x01, 0x9F, 0x98, 0x11, 0x92, 0x73, 0x31,
    0x09, 0x07, 0xE6, 0xCA, 0x0C, 0xC3, 0x66, 0xFE,
    0xC1, 0x59, 0xAE, 0xCE, 0xC2, 0x5E, 0xE5, 0xB5,
};

u8 g_kirk_ecdsa_buffer[0x64] __attribute__((aligned(64)));

typedef struct
{
    char magic[0x10]; // INFINITY UPDATE\0
    char r[0x14];
    char s[0x14];
} InfinityUpdate;

typedef struct
{
	int magic;
	int nsections;
	u32 sections[8];
} __attribute__((packed)) MFC_Header;

typedef struct
{
	char name[32];
	int size;
	int nfiles;
} __attribute__((packed)) MFC_Section;

typedef struct
{
	char path[128]; // 0
	int filesize; // 128
	int model; // 132
	int signcheck;
	int loadmod;
	int dir;
} __attribute__((packed)) MFC_FileEntry;

SceUID fd = -1;
int inited = 0;
int section_inited = 0;
MFC_Header header;
MFC_Section section;
int getfilecount = 0;

void ClearCaches(void)
{
	sceKernelIcacheClearAll();
	sceKernelDcacheWritebackAll();
}

unsigned int getBaryon(void)
{
	unsigned int baryon;

    pspSdkSetK1(0);
	sceSysconGetBaryonVersion(&baryon);
	return baryon;
}

int getTrueModel(void)
{
    pspSdkSetK1(0);

    int model = sceKernelGetModel();
	int true_model = model;

	/* check for real model if it claims it is a 04g (can be 09g) */
	if (model == 3)
	{
		/* get the baryon */
		u32 baryon = getBaryon();

		/* now get the determinating model */
		u32 det_model = (baryon >> 16) & 0xFF;

		/* now check if it is within range */
		if (det_model >= 0x2E && det_model < 0x30)
		{
			/* it's a 09g (or a sneaky 07g...) */
			if ((baryon >> 24) == 1)
			{
				/* 07g!! */
				true_model = 6;
			}
			else
			{
				/* 09g */
				true_model = 8;
			}
		}
	}

    return true_model;
}

int mfcOpen(char *file)
{
	int k1 = pspSdkSetK1(0);

	if (inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	fd = sceIoOpen(file, PSP_O_RDONLY, 0);

	if (fd < 0)
	{
		pspSdkSetK1(k1);
		return fd;
	}

	if (sceIoRead(fd, &header, sizeof(header)) != sizeof(header))
	{
		pspSdkSetK1(k1);
		return -1;
	}

	if (header.magic != MFC_MAGIC)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	inited = 1;
	section_inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcOpenSection(char *name)
{
	int i;
	int k1 = pspSdkSetK1(0);

	if (!inited || section_inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	for (i = 0; i < header.nsections; i++)
	{
		sceIoLseek(fd, header.sections[i], PSP_SEEK_SET);

		if (sceIoRead(fd, &section, sizeof(section)) != sizeof(section))
		{
			pspSdkSetK1(k1);
			return -3;
		}

		if (strcmp(section.name, name) == 0)
		{
			section_inited = 1;
			return 0;
		}
	}

	pspSdkSetK1(k1);
	return -1;
}

int mfcGetNextFile(char *file, int *size, int *cur, int *total, void *buf, int bufsize)
{
	int k1 = pspSdkSetK1(0);
    SceOff entryPos = 0;
	MFC_FileEntry entry;

	memset(&entry, 0, sizeof(entry));

	while (!(entry.model & (1 << sceKernelGetModel())))
	{
		if (getfilecount > section.nfiles)
		{
			pspSdkSetK1(k1);
			return -1;
		}

		if (entry.filesize)
		{
			sceIoLseek(fd, entry.filesize, PSP_SEEK_CUR);
		}

        entryPos = sceIoLseek(fd, 0, PSP_SEEK_CUR);

		if (sceIoRead(fd, &entry, sizeof(entry)) != sizeof(entry))
		{
			pspSdkSetK1(k1);
			return -1;
		}

		getfilecount++;
	}

	strcpy(file, entry.path);

	if (buf && bufsize)
	{
		if (entry.filesize > bufsize)
		{
            sceIoLseek(fd, entryPos, PSP_SEEK_SET);
            getfilecount--;
			pspSdkSetK1(k1);
			return -1;
		}

		if (sceIoRead(fd, buf, entry.filesize) != entry.filesize)
		{
			pspSdkSetK1(k1);
			return -1;
		}

		if (entry.signcheck)
		{
			pspSignCheck(buf);
		}
	}
    else if (size != NULL)
    {
        sceIoLseek(fd, entryPos, PSP_SEEK_SET);
        getfilecount--;
    }

    if (size)
    	*size = entry.filesize;

	if (cur)
		*cur = getfilecount;

	if (total)
		*total = section.nfiles;

	pspSdkSetK1(k1);
	return 0;
}

int mfcCloseSection()
{
	int k1 = pspSdkSetK1(0);

	getfilecount = 0;
	section_inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcClose()
{
	int k1 = pspSdkSetK1(0);

	if (!inited || section_inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	sceIoClose(fd);
	inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcInit(char *file)
{
	int k1 = pspSdkSetK1(0);

	if (inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	fd = sceIoOpen(file, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);

	if (fd < 0)
	{
		pspSdkSetK1(k1);
		return fd;
	}

	memset(&header, 0, sizeof(header));

	header.magic = MFC_MAGIC;

	if (sceIoWrite(fd, &header, sizeof(header)) != sizeof(header))
	{
		pspSdkSetK1(k1);
		return -1;
	}

	inited = 1;
	section_inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcAddDirectory(char *dir, int model)
{
	int k1 = pspSdkSetK1(0);
	MFC_FileEntry entry;

	if (!inited || !section_inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	if (strlen(dir) >= sizeof(entry.path))
	{
		pspSdkSetK1(k1);
		return -1;
	}

	memset(&entry, 0, sizeof(entry));
	strcpy(entry.path, dir);

	entry.dir = 1;
	entry.model = model;

	if (sceIoWrite(fd, &entry, sizeof(entry)) != sizeof(entry))
	{
		pspSdkSetK1(k1);
		return -1;
	}

	section.nfiles++;

	pspSdkSetK1(k1);
	return 0;
}

int mfcAddFile(char *file, void *buf, int size, int model, int signcheck)
{
	int k1 = pspSdkSetK1(0);
	MFC_FileEntry entry;

	if (!inited || !section_inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	if (!buf || !size || !file)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	if (strlen(file) >= sizeof(entry.path))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	memset(&entry, 0, sizeof(entry));
	strcpy(entry.path, file);

	entry.filesize = size;
	entry.model = model;
	entry.signcheck = signcheck;

	if (sceIoWrite(fd, &entry, sizeof(entry)) != sizeof(entry))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	if (sceIoWrite(fd, buf, size) < size)
	{
        pspSdkSetK1(k1);
		return -1;
	}

	section.nfiles++;

	pspSdkSetK1(k1);
	return 0;
}

int mfcInitSection(char *name)
{
	int k1 = pspSdkSetK1(0);

	if (!inited || section_inited)
	{
        pspSdkSetK1(k1);
		return -1;
	}

	if (header.nsections >= (sizeof(header.sections)/sizeof(u32)))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	if (strlen(name) >= sizeof(section.name))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	memset(&section, 0, sizeof(section));
	strcpy(section.name, name);

	header.sections[header.nsections] = sceIoLseek(fd, 0, PSP_SEEK_CUR);

	if (sceIoWrite(fd, &section, sizeof(section)) != sizeof(section))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	section_inited = 1;

	pspSdkSetK1(k1);
	return 0;
}

int mfcEndSection()
{
	int k1 = pspSdkSetK1(0);

	if (!inited || !section_inited)
	{
		pspSdkSetK1(k1);
		return -1;
	}

	section.size = sceIoLseek(fd, 0, PSP_SEEK_CUR)-header.sections[header.nsections]-sizeof(section);
	sceIoLseek(fd, header.sections[header.nsections], PSP_SEEK_SET);

	if (sceIoWrite(fd, &section, sizeof(section)) != sizeof(section))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	sceIoLseek(fd, 0, PSP_SEEK_END);

	header.nsections++;
	section_inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcEnd()
{
	int k1 = pspSdkSetK1(0);

	if (!inited || section_inited)
	{
        pspSdkSetK1(k1);
		return -1;
	}

	sceIoLseek(fd, 0, PSP_SEEK_SET);

	if (sceIoWrite(fd, &header, sizeof(header)) != sizeof(header))
	{
        pspSdkSetK1(k1);
		return -1;
	}

	sceIoClose(fd);
	inited = 0;

	pspSdkSetK1(k1);
	return 0;
}

int mfcLflashFatfmtStartFatfmt(int argc, void *argp)
{
	int k1 = pspSdkSetK1(0);

	int res = sceLflashFatfmtStartFatfmt(argc, argp);

	pspSdkSetK1(k1);
	return res;
}

UpdaterType verifyUpdate(const char *data, unsigned int size)
{
    u8 sha1[0x14];
    int k1 = pspSdkSetK1(0);

    InfinityUpdate *update = (InfinityUpdate *)data;

    // first verify magic
    if (memcmp(update->magic, UPDATE_MAGIC, 0x10) != 0)
    {
        pspSdkSetK1(k1);
        return INVALID_UPDATE;
    }
    // calculate SHA1
    sceKernelUtilsSha1Digest((u8 *)data+sizeof(InfinityUpdate), size-sizeof(InfinityUpdate), sha1);

    memcpy(g_kirk_ecdsa_buffer, g_davee_ecdsa_pub, 0x28);
    memcpy(g_kirk_ecdsa_buffer+0x28, sha1, 0x14);
    memcpy(g_kirk_ecdsa_buffer+0x3C, update->r, 0x14);
    memcpy(g_kirk_ecdsa_buffer+0x50, update->s, 0x14);

    // verify signature
    int res = sceUtilsBufferCopyWithRange(NULL, 0, g_kirk_ecdsa_buffer, 0x64, 0x11);

    pspSdkSetK1(k1);
    return res == 0 ? (OFFICIAL_UPDATE) : (UNOFFICIAL_UPDATE);
}

int mfcKernelLoadModuleBuffer(void *buffer, SceSize size, int flags, void *null)
{
    u32 k1 = pspSdkSetK1(0);
    int res = sceKernelLoadModuleBuffer(buffer, size, flags, null);
    pspSdkSetK1(k1);
    return res;
}

int module_start(SceSize args, void *argp)
{
    return 0;
}
