TARGET = infinity_kinstaller
OBJS = src/main.o src/sceSyscon_driver.o src/pspDecrypt_driver.o

INCDIR = include
CFLAGS = -Os -G0 -Wall -fno-pic -fshort-wchar
ASFLAGS = $(CFLAGS)

BUILD_PRX = 1
PRX_EXPORTS = src/exports.exp

PSP_FW_VERSION = 661

USE_KERNEL_LIBS=1
USE_KERNEL_LIBC=1

LIBDIR = lib
LDFLAGS = -nostartfiles
LIBS = -lpsplflash_fatfmt -lpspsemaphore

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
