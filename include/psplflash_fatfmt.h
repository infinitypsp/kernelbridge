/**
 * File for logical flash format
*/

#ifndef PSPLFLASHFATFMT_H_
#define PSPLFLASHFATFMT_H_

#include <pspsdk.h>

/**
 * Performs a logical format in a flash partition.
 *
 * @param argc - The number of parameters
 * @param argv - The parameters
 * @return < 0 on error
 */
int sceLflashFatfmtStartFatfmt(int argc, char *argv[]);

#endif // PSPLFLASHFATFMT_H_
